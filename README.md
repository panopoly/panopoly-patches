Panopoly Patches
================

When applying patches stored at a remote URL via
[composer](https://github.com/cweagans/composer-patches), it's important that
their content does not change unexpectedly.

Linking to patch files that were uploaded to Drupal.org is considered safe,
because it isn't possible for a user to replace the patch file, they can only
upload a new patch file, which will have a different URL.

However, now Drupal.org supports
[issue forks](https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/creating-issue-forks-and-merge-requests)
as an alternative to uploading patch files, which allows multiple contributes
to commit changes to a Git repo.

It's possible to make a URL that points to a patch file for the changes in the
issue fork, but **it is NOT safe!** The code at the issue fork could be
changed later by a malicious actor.

For this reason, the Panopoly project will copy any patches from issue forks
into this repository and link to them here, because:

1. We can formulate the links such that they include the Git commit hash, and
   so the contents of the patch file cannot be easily changed.

2. We are in control of who has permission to edit this Git repo.

